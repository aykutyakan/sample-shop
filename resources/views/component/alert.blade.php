@if(isset(session('message')["success"]))
<div class="alert alert-success" role="alert" style="position:relative">
    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
    <span><b> Başarılı - </b> {{ session('message')["success"] }} </span>
</div>
@endif
@if(isset(session('message')["error"]))
<div class="alert alert-danger" role="alert" style="position:relative">
    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
    <span><b> Hata - </b> {{ session('message')["error"] }} </span>
</div>
@endif
@if(isset(session('message')["warning"]))
<div class="alert alert-warning" role="alert" style="position:relative">
    <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
    <span><b> Uyarı - </b> {{ session('message')["warning"] }} </span>
</div>
@endif
@if($errors->any())
    <div class="alert alert-danger" role="alert" style="position:relative">
        <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
        <span><b> Hata</b>   {{ $errors->first() }} </span>
    </div>    
@endif