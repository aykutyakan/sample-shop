<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <form action="{{route('login.auth')}}" method='POST'>
    @csrf
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="loginModalLabel">Giriş Yap</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="email" class="col-form-label">Email Adresiniz:</label>
              <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
              <label for="password" class="col-form-label">Şifreniz:</label>
              <input type="password" class="form-control" id="password" name="password">
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <small><a href="#" role="button" data-toggle="modal" data-target="#singupModal"> Eğer üye değilseniz lütfen kayıt olunuz.</a></small>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Vazgeç</button>
          <button type="submit" class="btn btn-primary">Giriş Yap</button>
        </div>
      </div>
    </div>
  </form>
</div>
  