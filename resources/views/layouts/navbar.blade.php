<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Sample</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    @guest
    <!-- Guest -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a href="#" role="button"  class="nav-link" data-toggle="modal" data-target="#loginModal">Giriş Yap</a>
      </li>
      <li class="nav-item">
        <a href="#" role="button"  class="nav-link" data-toggle="modal" data-target="#singupModal">Kayıt Ol</a>
      </li>
    </ul>
    <!-- Guest -->
    @endguest
    @can('isMember')
    <!-- Member -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a href="{{route('main.product.index')}}" class="nav-link">Anasayfa</a>
      </li>
      <li class="nav-item">
        <a href="{{route('main.basket.index')}}" class="nav-link">Sepetim</a>
      </li>
      <li class="nav-item">
        <a href="{{route('main.order.index')}}" class="nav-link">Siparişlerim</a>
      </li>
      <li class="nav-item">
        <a href="{{route('login.logout')}}" class="nav-link">Çıkış Yap</a>
      </li>
    </ul>
    <!-- Member -->
    @endcan
    @can('isAdmin')
    <!-- Admin -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="{{route('admin.product.index')}}" class="nav-link">Ürünler</a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.category.index')}}" class="nav-link">Kategoriler</a>
      </li>
      <li class="nav-item">
        <a href="{{route('admin.order.index')}}" class="nav-link">Siparişler</a>
      </li>
      <li class="nav-item">
        <a href="{{route('login.logout')}}" class="nav-link">Çıkış Yap</a>
      </li>
    </ul>
    <!-- Admin -->
    @endcan
  </div>
</nav>