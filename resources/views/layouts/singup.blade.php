<div class="modal fade" id="singupModal" tabindex="-1" role="dialog" aria-labelledby="singupModalLabel" aria-hidden="true">
    <form action="{{route('user.singup')}}" method='POST'>
      @csrf
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="singupModalLabel">Üye Ol</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="name" class="col-form-label">Kullanıcı Adı:</label>
                <input required minlength="3" type="text" class="form-control" id="name" name="name">
              </div>
              <div class="form-group">
                <label for="email" class="col-form-label">Email:</label>
                <input required type="email" class="form-control" id="email" name="email">
              </div>
              <div class="form-group">
                <label for="password" class="col-form-label">Şifreniz:</label>
                <input required minlength="6" type="password" class="form-control" id="password" name="password">
              </div>
              <div class="form-group">
                <label for="password_confirmation" class="col-form-label">Şifrenizi Tekrar Giriniz:</label>
                <input required minlength="6" type="password" class="form-control" id="password_confirmation" name="password_confirmation">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <small><a href="#"> Eğer üyeyseniz lütfen giriş yapınız.</a></small>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Vazgeç</button>
            <button type="submit" class="btn btn-primary">Kayıt Ol</button>
          </div>
        </div>
      </div>
    </form>
  </div>
    