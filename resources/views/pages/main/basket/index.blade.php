@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">
      @if($baskets->count())
      <div class="row mb-5 align-items-center">
          <div class="col-8">
              <p>Sepetinizde toplam {{$baskets->count()}} adet ürün bulunmaktadır.</p>
              <p> Ürünlerinizin toplam ücreti indirimler dahil {{$baskets->sum('totalPriceWithDiscount')}}₺ </p>
          </div>
          <div class="col-4 text-right"> <a class="btn btn-info" href="{{route('main.order.checkout')}}">Alışverişi tamamla</a> </div>
      </div>
      @else 
      <div class="row mb-5 align-items-center">
        <div class="col-8">
            <p>Sepetinizde hiç ürün bulunmamaktadır.</p>
            
        </div>
    </div>
      @endif
      <div class="row">
        @foreach($baskets as $basket)
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="{{Storage::url($basket->product->cover)}}" alt="Card image cap">
            <div class="card-body">
              <a href="#" class="text-decoration-none">
                <p class="h6 text-bold text-muted">
                  {{$basket->product->name}}
                </p>
                <p class="card-text text-dark">{{$basket->product->ShortDescription}}</p>
                <p class="card-text text-dark">İstenilen / Toplam Adet Sayısı :{{$basket->count}} / {{$basket->product->stock}}</p>
              </a>
              <div class="d-flex justify-content-between align-items-center mt-2">
                <div class="btn-group">
                    <a  href="{{route('main.basket.drop', $basket->product_id)}}" class="btn btn-outline-danger">Çıkar</a>
                    <a href="{{route('main.basket.add', $basket->product_id)}}" class="btn btn-outline-warning">Ekle</a>
                </div>
                <p class="m-0">
                  @if($basket->product->discount)
                  <small style="text-decoration:line-through;" class="text-muted h6">{{$basket->totalPrice}}₺</small>
                  <span class="h3 text-dark"> 
                    {{$basket->totalPriceWithDiscount}}₺
                  </span>
                  @else
                  <span class="h3 text-dark"> 
                    {{$basket->totalPrice}}₺
                  </span>
                  @endif
                </p>
              </div>
            </div>
          </div>
        </div>
        @endforeach       
      </div>
    </div>
  </div>
@endsection