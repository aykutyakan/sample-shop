@extends("layouts.app")

@section('content')
<div class="container">
  <div class="row mt-5">
    <div class="col-md-4 order-md-2 mb-4">
      <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-muted">Sepetiniz</span>
        <span class="badge badge-secondary badge-pill">{{$baskets->count()}}</span>
      </h4>
      <ul class="list-group mb-3">
        @foreach($baskets as $basket)
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0"> {{$basket->product->name}} </h6>
            <small class="text-muted">{{$basket->count}} Adet</small>
          </div>
          <span class="text-muted">{{$basket->totalPriceWithDiscount}}₺ </span>
        </li>
        @endforeach
        <li class="list-group-item d-flex justify-content-between">
          <span>Toplam (TL)</span>
          <strong>{{$baskets->sum('totalPriceWithDiscount')}}₺</strong>
        </li>
      </ul>
    </div>
    <div class="col-md-8 order-md-1">
      <h4 class="mb-3"> Adres bilgileriniz</h4>
      <form class="needs-validation" novalidate method="POST" action="{{route('main.order.payment')}}">
        @csrf
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="buyed_name">Adınız</label>
            <input name="buyed_name" type="text" class="form-control" id="firstName" placeholder="Adnız" value="" required>
            <div class="invalid-feedback">
              İsim alanı geçersiz.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="buyed_lastname">Soyadınız</label>
            <input name="buyed_lastname" type="text" class="form-control" id="lastName" placeholder="Soyadınız" value="" required>
            <div class="invalid-feedback">
              Soyisim alanı geçersiz
            </div>
          </div>
        </div>

        <div class="mb-3">
          <label for="username">Telefon No.</label>
          <div class="input-group">
            <input type="tel" name="phone" class="form-control" id="phone" placeholder="Telefon Numaranız." required>
            <div class="invalid-feedback" style="width: 100%;">
              Lütfen telefon numaranızı doğru giriniz
            </div>
          </div>
        </div>
        <div class="mb-3">
          <label for="address">Adres</label>
          <input type="text" name="address" class="form-control" id="address" placeholder="1234 Main St" required>
          <div class="invalid-feedback">
            Lütfen adresinizi doğru giriniz
          </div>
        </div>


        <div class="row">
          <div class="col-md-4 mb-3">
            <label for="state">Şehir</label>
            <select name="city" class="custom-select d-block w-100" id="state" required>
              <option selected value="">Seçiniz</option>
              <option value="istanbul">İstanbul</option>
              <option value="ankara">Ankara</option>
              <option value="izmir">İzmir</option>
            </select>
            <div class="invalid-feedback">
              Lütfen şehir seçiniz.
            </div>
          </div>
          <div class="col-md-3 mb-3">
            <label for="post_code">Posta kodu</label>
            <input name="post_code" type="text" class="form-control" id="post_code" placeholder="" required>
            <div class="invalid-feedback">
              Lütfen posta kodunuzu doğru giriniz.
            </div>
          </div>
        </div>
        
        <hr class="mb-4">

        <h4 class="mb-3">Ödeme Formu</h4>

        <div class="d-block my-3">
          <div class="custom-control custom-radio">
            <input id="credit" name="payment_type" value="{{PaymentType::CREDIT}}" type="radio" class="custom-control-input" checked required>
            <label class="custom-control-label" for="credit">Kredi Kartı</label>
          </div>
          <div class="custom-control custom-radio">
            <input id="transfer" name="payment_type" value="{{PaymentType::TRANSFER}}" type="radio" class="custom-control-input" required>
            <label class="custom-control-label" for="transfer">Havale/Eft</label>
          </div>
          <div class="custom-control custom-radio">
            <input id="debit" name="payment_type" value="{{PaymentType::DEBIT}}" type="radio" class="custom-control-input" required>
            <label class="custom-control-label"  for="debit">Nakit Kart</label>
          </div>
        </div>
        <hr class="mb-4">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Ödemeyi Tamamla</button>
      </form>
    </div>
  </div>
</div>
@endsection