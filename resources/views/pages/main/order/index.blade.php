@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
        <!-- Acordion-->
        <div class="accordion w-100" id="accordionExample">
          @foreach($orders as $key => $order)
          <div class="card">
            <div class="card-header" id="heading{{$key}}">
              <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                  {{$order->created_at}} 
                  <span class="text-muted">
                    tarihinde toplam {{$order->orderProducts->count()}} adet ürün siparişi verildi.
                  </span>
                </button>
                @if($order->is_approved == OrderStatus::APPROVED )
                <small class="float-right text-success">
                  Alındı
                </small>
                  @elseif($order->is_approved == OrderStatus::UNAPPROVED)
                  <small class="float-right text-danger">
                    İptal Edildi
                  </small>
                  @elseif($order->is_approved == OrderStatus::PROCCES)
                  <small class="float-right text-warning">
                    Beklemede
                  </small>
                  @endif
              </h2>
            </div>
        
            <div id="collapse{{$key}}" class="collapse show" aria-labelledby="heading{{$key}}" data-parent="#accordionExample">
              <div class="card-body">
                <ul class="list-group mb-3">
                  @foreach($order->orderProducts as $product)
                  <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                      <h6 class="my-0"> {{$product->product->name}} </h6>
                      <small class="text-muted">{{$product->count}} Adet</small>
                    </div>
                    <span class="text-muted">{{$product->price}}₺ </span>
                  </li>
                  @endforeach
                  <li class="list-group-item d-flex justify-content-between">
                    <span>Toplam (TL)</span>
                    <strong>{{$order->orderProducts->sum('price')}}₺</strong>
                  </li>
                </ul>

              </div>
            </div>
          </div>
          @endforeach
        </div>
        <!-- END Acardion-->
      </div>
    </div>
  </div>
@endsection