@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">
      <div class="row">
        <!-- Product Img-->
        <div class="col-6">
            <img class="card-img-top" src="{{Storage::url($product->cover)}}" alt="{{$product->name}}">
        </div>
        <!-- Product Img-->
        <!-- Product Detail-->
        <div class="col-6">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <h4> Ürün Bilgileri</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-4"> Ürün Adı </div>
                        <div class="col-8 "> {{$product->name}} </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-4"> Ürün Kategorisi </div>
                        <div class="col-8"> {{$product->category->name}}</div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-4"> Fiyat </div>
                        <div class="col-8"> 
                            @if($product->discount)
                            <small style="text-decoration:line-through;" class="text-muted h6">{{$product->price}}₺</small>
                            <span class="h3 text-dark"> 
                                {{$product->priceWithDiscount}}₺
                            </span>
                            @else
                            <span class="h3 text-dark"> 
                                {{$product->price}}₺
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-4">Kalan Stok Miktarı</div>
                        <div class="col-8">
                            <p class="card-text text-dark">{{$product->stock}}</p>
                        </div>
                    </div>
                    @can('isMember')
                    <form action="{{route('main.basket.add', $product->id)}}" method="GET">
                            <div class="row mt-3">
                            <div class="col-4">
                                <input placeholder="Adet" type="number" name="count" class="form-control"/>
                            </div>
                            <div class="col-8"> 
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="submit" class="btn btn-outline-warning">Sepete Ekle</button>
                                        <button type="submit" formaction="{{route('main.basket.directBuy', $product->id)}}" class="btn btn-info">Satın Al</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    @else
                    <div class="row mt-2">
                        <div class="col-12 text-center">
                            <span class="text-muted">
                                Satın almak için lütfen giriş yapınız.
                            </span>
                        </div>
                    </div>
                    @endcan
                    <div class="row mt-4">
                        <div class="col-4">Açıklama</div>
                        <div class="col-8">
                            <p class="card-text text-dark">{{$product->description}}</p>
                        </div>
                    </div>

                </div>
              </div>
        </div>
        <!-- Produt Detail-->
      </div>


      
    </div>
  </div>
@endsection