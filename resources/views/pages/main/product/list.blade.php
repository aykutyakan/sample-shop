@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
        @foreach($products as $product)
        <div class="col-md-4">
          <div class="card mb-4 box-shadow">
            <img class="card-img-top" src="{{Storage::url($product->cover)}}" alt="Card image cap">
            <div class="card-body">
              <a href="#" class="text-decoration-none">
                <p class="h6 text-bold text-muted">
                  {{$product->name}}
                </p>
                <p class="card-text text-dark">
                  {{$product->ShortDescription}}
                </p>
                <p>
                  <a href="{{route('main.product.show', $product->id)}}">Ürünü görüntüle</a>
                </p>
                <p class="card-text text-dark"> Kalan adet miktarı: {{$product->stock}} </p>
              </a>
              <div class="d-flex justify-content-between align-items-center mt-2">
                @can('isMember')
                <div class="btn-group">
                    <a href="{{route('main.basket.add', $product->id)}}" type="button" class="btn btn-outline-warning">Sepete Ekle</a>
                    <a href="{{route('main.basket.directBuy', $product->id)}}" type="button" class="btn btn-info">Satın al</a>
                </div>
                @else
                 <small class="card-text text-muted">Satın almak için üye girişi yapınız</small>
                @endcan
                <p class="m-0">
                  @if($product->discount)
                  <small style="text-decoration:line-through;" class="text-muted h6">{{$product->price}}₺</small>
                  <span class="h3 text-dark"> 
                    {{$product->priceWithDiscount}}₺
                  </span>
                  @else
                  <span class="h3 text-dark"> 
                    {{$product->price}}₺
                  </span>
                  @endif
                </p>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        
      </div>

      <div class="row">
        <div class="col-12">
          {{$products->links()}}
        </div>
      </div>
    </div>
  </div>
@endsection