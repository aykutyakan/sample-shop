@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
        <!-- Acordion-->
        <div class="accordion w-100" id="accordionExample">
          @foreach($orders as $key => $order)
          <div class="card">
            <div class="card-header" id="heading{{$key}}">
              <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
                  {{$order->created_at}} 
                  <span class="text-muted">
                    tarihinde toplam {{$order->orderProducts->count()}} adet ürün siparişi verildi.
                  </span>
                </button>
                @if($order->is_approved == OrderStatus::APPROVED )
                <small class="float-right text-success">
                  Alındı
                </small>
                  @elseif($order->is_approved == OrderStatus::UNAPPROVED)
                  <small class="float-right text-danger">
                    İptal Edildi
                  </small>
                  @elseif($order->is_approved == OrderStatus::PROCCES)
                  <small class="float-right text-warning">
                    Beklemede
                  </small>
                  @endif
              </h2>
            </div>
        
            <div id="collapse{{$key}}" class="collapse" aria-labelledby="heading{{$key}}" data-parent="#accordionExample">
              <div class="card-body">
                <div class="row py-2 bg-light">
                  <div class="col-2">Satın alan kullanıcı</div>
                  <div class="col-2">{{$order->user->name}}</div>
                  <div class="col-2">Email adresi</div>
                  <div class="col-2">{{$order->user->email}}</div>
                  <div class="col-2">Alıcı Adı</div>
                  <div class="col-2">{{$order->fullBuyedName}}</div>
                </div><div class="row py-2 bg-dark text-light">
                  <div class="col-2">Şehir</div>
                  <div class="col-2">{{$order->city}}</div>
                  <div class="col-2">Posta kodu</div>
                  <div class="col-2">{{$order->post_code}}</div>
                  <div class="col-2">Telefon</div>
                  <div class="col-2">{{$order->phone}}</div>
                </div><div class="row py-2 bg-light">
                  <div class="col-2">Adres</div>
                  <div class="col-10">{{$order->address}}</div>
                </div>
                <ul class="list-group mb-3">
                  @foreach($order->orderProducts as $product)
                  <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                      <h6 class="my-0"> {{$product->product->name}} </h6>
                      <small class="text-muted">{{$product->count}} Adet</small>
                    </div>
                    <span class="text-muted">{{$product->price}}₺ </span>
                  </li>
                  @endforeach
                  <li class="list-group-item d-flex justify-content-between">
                    <span>Toplam (TL)</span>
                    <strong>{{$order->orderProducts->sum('price')}}₺</strong>
                  </li>
                </ul>
                @if($order->is_approved == OrderStatus::PROCCES)
                <div class="btn-group">
                  <a href="{{route('admin.order.unapproved', $order->id)}}" class="btn btn-danger">İptal Et</a>
                  <a href="{{route('admin.order.approved', $order->id)}}" class="btn btn-success">Tamamla</a>
                </div>
                @endif
              </div>
            </div>
          </div>
          @endforeach
        </div>
        <!-- END Acardion-->
      </div>
      <div class="row">
        <div class="col-12">
          {{$orders->links()}}
        </div>
      </div>
    </div>
  </div>
@endsection