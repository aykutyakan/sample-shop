@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">
      <div class="row mb-5 align-items-center">
          <div class="col-12">
            <p>Sayfanızdaki ürünlerinizi aşağıda listelenmekltedir.Yeni ürün eklemek için lütfen 
              <a href="{{route('admin.product.index')}}">
                tıklayınız.
              </a>
            </p>
          </div>
      </div>
      <div class="row">
        <div class="col-12">
            <form method="POST" action="{{route('admin.product.update', $product->id)}}" enctype="multipart/form-data">
              @csrf
                <div class="form-group">
                  <label for="name">Ürün Adı*</label>
                  <input value="{{$product->name}}"  type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Ürün adı">
                  <small id="nameHelp" class="form-text text-muted">Anasayfanızda ürün başlığı olarak gösterilecek</small>
                </div>
                <div class="form-group">
                    <label for="category">Kategori*</label>
                    <select name="category_id" id="category" class="form-control" aria-describedby="categoryHelp">
                        <option disabled value="">Ürün kategorisi</option>
                        @foreach($categories as $category)
                          <option @if($product->category_id == $category->id) checked @endif value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    <small id="categoryHelp" class="form-text text-muted">Ürünüzün bağlı olacağı kategori seçmek zorundasınız.</small>
                </div>
                <div class="form-group">
                    <label for="cover">Ürün resmi*</label>
                    <input type="file" class="form-control-file" name="cover" id="cover" aria-describedby="coverHelp">
                    <input type="hidden" name="cover_old_path" value="{{$product->cover}}"/> 
                    <small id="coverHelp" class="form-text text-muted">Ürününüze ait resim anasayfanızda gösterilecek</small>
                </div>
                <div class="form-group">
                  <label for="stock">Ürün Adedi*</label>
                  <input value="{{$product->stock}}" type="number" class="form-control" name="stock" id="stock" aria-describedby="stockHelp">
                  <small id="stockHelp" class="form-text text-muted">Ürününüze ait stok bilgisi</small>
                </div>
                <div class="form-group">
                    <label for="price">Ürün fiyatı*</label>
                    <input value="{{$product->price}}" type="number" class="form-control" name="price" id="price" aria-describedby="priceHelp">
                    <small id="priceHelp" class="form-text text-muted">Ürünün vergiler dahil toplam fiyatı </small>
                </div>
                <div class="form-group">
                    <label for="discount">Ürün İndirim oranı</label>
                    <input value="{{$product->discount}}" type="number" class="form-control" name="discount" id="discount" aria-describedby="discountHelp">
                    <small id="discountHelp" class="form-text text-muted">Varsa ürününe uygulanacak indirim oranı</small>
                </div>
                <div class="form-group">
                  <label for="description">Ürün açıklaması*</label>
                  <textarea class="form-control" name="description" id="description" cols="30" rows="10" aria-describedby="descriptionHelp">{{$product->description}}</textarea>
                  <small id="descriptionHelp" class="form-text text-muted">Ürüne ait özellikleri lütfen yazınız</small>
              </div>
                <button type="submit" class="btn btn-primary">Güncelle</button>
            </form>              
        </div>
      </div>
    </div>
  </div>
@endsection