@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">
      <div class="row mb-5 align-items-center">
          <div class="col-12">
            <p>Ürünlerinizin bağlı olduğu kategoriler aşağıda listelenmektedir. Yeni kategori eklemek için lütfen 
              <a href="{{route('admin.category.create')}}">
                tıklayınız.
              </a>
            </p>
          </div>
      </div>
      <div class="row">
        <div class="list-group w-75">
          @foreach($categories as $category)
          
          <div class="list-group-item list-group-item-action flex-column align-items-start">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Kategori Adı: {{$category->name}} </h5>
            </div>
            <p class="mb-2">Kategori Etiketi: {{$category->label}} </p>
            <div class="btn-group">
              <a href="{{route('admin.category.destroy', $category->id)}}" class="btn btn-sm btn-danger">Sil</a>
              <a href="{{route('admin.category.edit', $category->id)}}" class="btn btn-sm btn-warning">Güncelle</a>
            </div>
          </div>
          @endforeach
          <div class="list-group-item list-group-item-action flex-column align-items-start d-none">
            <div class="d-flex w-100 justify-content-between">
              <h5 class="mb-1">Kategori Adı: Pantolon</h5>
            </div>
            <p class="mb-1">Kategori Etiketi: pantolon</p>
            <div class="btn-group">
              <a href="#" class="btn btn-sm btn-danger">Sil</a>
              <a href="#" class="btn btn-sm btn-warning">Güncelle</a>
            </div>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-12">
          {{$categories->links()}}
        </div>
      </div>
    </div>
  </div>
@endsection