@extends("layouts.app")

@section('content')
  <div class="album py-5 bg-light">
    <div class="container">
      <div class="row mb-5 align-items-center">
          <div class="col-12">
            <p>Sayfanızdaki kategoriler aşağıda listelenmekltedir. Tüm kategorileri görmek için 
              <a href="{{route('admin.category.index')}}">
                tıklayınız.
              </a>
            </p>
          </div>
      </div>
      <div class="row">
        <div class="col-12">
            <form action="{{route('admin.category.store')}}" method="POST">
              @csrf
                <div class="form-group">
                  <label for="name">Kategori Adı*</label>
                  <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Kategori adı">
                  <small id="nameHelp" class="form-text text-muted">Kategori adı kullanıcının gördüğü isim olacaktır.  </small>
                </div>
                <div class="form-group">
                    <label for="label">Kategori Etiketi*</label>
                    <input type="text" class="form-control" name="label" id="label" aria-describedby="labelHelp" placeholder="Kategori etiketi">
                    <small id="labelHelp" class="form-text text-muted">Kategori etiketini türkçe karakterler ve boşluk kullanmayınız. Boşluk yerine tire(-) kullanınız.  Örnek : (ornek-etiket)</small>
                </div>
                <button type="submit" class="btn btn-primary">Kaydet</button>
            </form>              
        </div>
      </div>
    </div>
  </div>
@endsection