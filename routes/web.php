<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/login', 'LoginController@auth')->name('login.auth');
Route::get('/logout', 'LoginController@logout')->name('login.logout');
Route::post('/singup', 'Main\UserController@singup')->name('user.singup');


Route::get('/',  "Main\ProductController@index")->name('main.product.index');
Route::get('product/{id}',  "Main\ProductController@show")->name('main.product.show');
Route::group(['middleware' => 'can:isMember', 'prefix' => '/', 'namespace' => 'Main'], function(){
    
    Route::get('product/{id}/buy',  "ProductController@buy")->name('main.product.buy');
    Route::get('product/{id}/basket',  "ProductController@basket")->name('main.product.basket');

    Route::get('order',  "OrderController@index")->name('main.order.index');

    Route::get('basket',  "BasketController@index")->name('main.basket.index');
    Route::get('basket/{productId}/add',  "BasketController@add")->name('main.basket.add');
    Route::get('basket/{productId}/drop',  "BasketController@drop")->name('main.basket.drop');
    Route::get('basket/{productId}/direct-buy',  "BasketController@directBuy")->name('main.basket.directBuy');
    
    Route::get('order/',  "OrderController@index")->name('main.order.index');
    Route::get('order/checkout',  "OrderController@checkout")->name('main.order.checkout');
    Route::post('order/payment',  "OrderController@payment")->name('main.order.payment');
});

Route::group(['middleware' => 'can:isAdmin', 'prefix' => '/admin', 'namespace' => 'Admin'], function(){
    Route::get('product', 'ProductController@index')->name('admin.product.index');
    Route::get('product/create', 'ProductController@create')->name('admin.product.create');
    Route::post('product/store', 'ProductController@store')->name('admin.product.store');
    Route::get('product/edit/{id}', 'ProductController@edit')->name('admin.product.edit');
    Route::post('product/update/{id}', 'ProductController@update')->name('admin.product.update');
    Route::get('product/destroy/{id}', 'ProductController@destroy')->name('admin.product.destroy');

    Route::get('order', 'OrderController@index')->name('admin.order.index');
    Route::get('order/approved/{id}', 'OrderController@approved')->name('admin.order.approved');
    Route::get('order/unapproved/{id}', 'OrderController@unapproved')->name('admin.order.unapproved');

    Route::get('category', 'CategoryController@index')->name('admin.category.index');
    Route::get('category/create', 'CategoryController@create')->name('admin.category.create');
    Route::post('category/store', 'CategoryController@store')->name('admin.category.store');
    Route::get('category/edit/{id}', 'CategoryController@edit')->name('admin.category.edit');
    Route::post('category/update/{id}', 'CategoryController@update')->name('admin.category.update');
    Route::get('category/destroy/{id}', 'CategoryController@destroy')->name('admin.category.destroy');

});