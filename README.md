## Kurulum
```sh
$ git clone https://bitbucket.org/aykutyakan/sample-shop/src/master/ .
$ compoer install
$ mv .env.example .env
$ php artisan key:generate
```
## Veritabanı kurlumu
Veritabanı bilgilerini .env dosyasına yazalım ve sonra aşağıdaki komutları çalıştıralım

```sh
$ php artisan migrate
$ php artisan db:seed
```

## Sisteme Kullanımı
Yönetici girişi için
email :admin@mail.com
password: password

Üye girişi için
email :uye@mail.com
password: password

## Çalıştırma
- `php artisan serve` 
