<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    //
    protected $fillable = [
        'product_id', 'price', 'count',
    ];

    protected $with = ["product"];
    public function product() {
        return $this->belongsTo(Product::class);
    }
}
