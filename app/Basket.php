<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'product_id',
    ];
    
    protected $with = ["product", "user"];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function getTotalPriceWithDiscountAttribute()
    {
        return $this->product->priceWithDiscount * $this->count;
    }
    public function getTotalPriceAttribute()
    {
        return $this->product->price * $this->count;
    }
}
