<?php

namespace App\Enum;

abstract class PaymentType extends EnumAbstract {
    const CREDIT = 1;
    const DEBIT = 2;
    const TRANSFER = 3;
    const PAYPAL = 4;
}
