<?php

namespace App\Enum;

abstract class OrderStatus extends EnumAbstract {
    const APPROVED = 1;
    const UNAPPROVED = 2;
    const PROCCES = 0;
}
