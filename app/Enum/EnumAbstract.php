<?php
namespace App\Enum;

abstract class EnumAbstract {
    static function getKeys(){
        $class = new \ReflectionClass(get_called_class());
        return array_keys($class->getConstants());
    }

}