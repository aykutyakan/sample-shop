<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind("App\Repository\CategoryRepository\ICategoryRepository", "App\Repository\CategoryRepository\ElequentCategoryRepository");
        $this->app->bind("App\Repository\ProductRepository\IProductRepository", "App\Repository\ProductRepository\ElequentProductRepository");
        $this->app->bind("App\Repository\OrderRepository\IOrderRepository", "App\Repository\OrderRepository\ElequentOrderRepository");
        $this->app->bind("App\Repository\BasketRepository\IBasketRepository", "App\Repository\BasketRepository\ElequentBasketRepository");
        $this->app->bind("App\Repository\UserRepository\IUserRepository", "App\Repository\UserRepository\ElequentUserRepository");
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
