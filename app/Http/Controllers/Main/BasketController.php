<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Repository\BasketRepository\IBasketRepository;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    private $basketRepositoryService;

    public function __construct(IBasketRepository $basketRepositoryService)
    {
        $this->basketRepositoryService = $basketRepositoryService;
    }

    public function index() {
        $data["baskets"]= $this->basketRepositoryService->all();
        return view('pages.main.basket.index', $data);
    }

    public function add($productId, Request $request)
    {
        $count = $request->count ?? 1;
        $result = $this->basketRepositoryService->add($productId, $count);
        $message = $result 
                    ? ["success" => "Ürün başarılı bir şekilde sepete eklendi"] 
                    : ["error" => "Ürün sepete eklenme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return back()->with('message', $message);
    }
    public function directBuy($productId, Request $request)
    {
        $count = $request->count ?? 1;
        $result = $this->basketRepositoryService->add($productId, $count);
        $message = $result 
                    ? ["success" => "Ürün başarılı bir şekilde sepete eklendi"] 
                    : ["error" => "Ürün sepete eklenme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return $result 
                ? redirect()->route('main.order.checkout')
                : back()->with('message', $message);
    }
    public function drop($productId) 
    {
        $count = 1;
        $result = $this->basketRepositoryService->drop($productId, $count);
        $message = $result 
                    ? ["success" => "Ürün başarılı bir şekilde sepete eksiltildi ve ya çıkarıldı."] 
                    : ["error" => "Sepeten ürün çıkarma sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return back()->with('message', $message);
    }
}
