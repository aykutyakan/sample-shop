<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Repository\ProductRepository\IProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $productRepositoryService;

    public function __construct(IProductRepository $productRepositoryService)
    {
        $this->productRepositoryService = $productRepositoryService;
    }

    public function index() {
        $data["products"] = $this->productRepositoryService->list();
        return view('pages.main.product.list', $data);
    }
    public function show($id) {
        $data["product"] = $this->productRepositoryService->find($id);
        return view('pages.main.product.show', $data);
    }
}
