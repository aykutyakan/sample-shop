<?php

namespace App\Http\Controllers\Main;

use App\Enum\PaymentType;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Repository\BasketRepository\IBasketRepository;
use App\Repository\OrderRepository\IOrderRepository;
use App\Repository\ProductRepository\IProductRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $basketRepositoryService;
    private $orderRepositoryService;
    private $productRepositoryService;
    public function __construct(
        IBasketRepository $basketRepositoryService,
        IOrderRepository $orderRepositoryService,
        IProductRepository $productRepositoryService
    )
    {
        $this->basketRepositoryService = $basketRepositoryService;
        $this->orderRepositoryService  = $orderRepositoryService;
        $this->productRepositoryService = $productRepositoryService;
    }
    
    public function index() {
        $data["orders"]= $this->orderRepositoryService->list();
        
        return view('pages.main.order.index', $data);
    }

    public function checkout() {
        $data["baskets"]= $this->basketRepositoryService->all();
        return view('pages.main.order.checkout', $data);
    }

    public function payment(OrderRequest $request) {
        
        if($request->payment_type == PaymentType::TRANSFER){
            $baskets= $this->basketRepositoryService->all();
            $insufficientProduct = $this->productRepositoryService->checkProductStock($baskets);
            if($insufficientProduct)
            {
                $message = ["error" => $insufficientProduct->product->name. "ürün istenilen stok adedinde mevcut değildir."];
            } else {   
                $res = $this->orderRepositoryService->order($request->all(), $baskets);
                if($res) {
                    $this->productRepositoryService->decreaseProductsCount($baskets);
                    $this->basketRepositoryService->flush();
                }
                $message = $res 
                ? ["success" => "Siparişiniz işleme alındı"]
                : ["error" => "Sistemde hata oluştu tekrar deneyiniz"];
            }
            return redirect()->route('main.order.checkout')->with("message", $message);
        }else 
            return back()->with("message", ["error" => "Şuan içi ödeme tipi sadece HAVAVE\EFT geçerli"]);
        
    }

}
