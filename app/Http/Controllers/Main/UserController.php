<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Repository\UserRepository\IUserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{

    private $userRepository;
    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function singup(UserRequest $request)
    {
        $result = $this->userRepository->singupMember($request->all());
        $message = $result 
                    ? ["success" => "Üyelik işleminiz yapıldı. Giriş yapınız."] 
                    : ["error" => "Üye ekleme işleminde hata oluştu. Tekrar deneyiniz"];
        
        return redirect()->route('main.product.index')->with('message', $message);
    }
}
