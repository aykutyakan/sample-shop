<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{

    public function auth(LoginRequest $request)
    {
        $credentials = [
            "email"=> $request->email,
            "password"=> $request->password,
        ];
        if(Auth::attempt($credentials))
            return redirect()->route('main.product.index');
        else 
            return back()->with('message', ['error' => 'Kullanıcı bilgileriniz hatalı']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('main.product.index');
    }
}
