<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Repository\CategoryRepository\ICategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $repositoryService;
    public function __construct(ICategoryRepository $repositoryService)
    {
        $this->repositoryService = $repositoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["categories"] = $this->repositoryService->list();
        
        return view('pages.admin.category.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $result = $this->repositoryService->store($request->all());
        $message = $result 
                    ? ["success" => "Kategori başarılı bir şekilde eklendi"] 
                    : ["error" => "Kategori eklenme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return redirect()->route('admin.category.create')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["category"] = $this->repositoryService->find($id);
        return view('pages.admin.category.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $result = $this->repositoryService->update($id, $request->all());
        $message = $result 
                    ? ["success" => "Kategori başarılı bir şekilde güncellendi"] 
                    : ["error" => "Kategori güncelleme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return redirect()->route('admin.category.edit', $id)->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->repositoryService->destroy($id);
        $message = $result 
                    ? ["success" => "Kategori başarılı bir şekilde silindi"] 
                    : ["error" => "Kategori silme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return redirect()->route('admin.category.index')->with('message', $message);   
    }
}
