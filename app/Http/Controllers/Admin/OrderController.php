<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\OrderRepository\IOrderRepository;
use App\Repository\ProductRepository\IProductRepository;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $productRepositoryService;
    private $orderRepositoryService;
    public function __construct(
        IProductRepository $productRepositoryService,
        IOrderRepository $orderRepositoryService
    )
    {
        $this->productRepositoryService = $productRepositoryService;
        $this->orderRepositoryService = $orderRepositoryService;
    }
    public function index()
    {
        $data["orders"]= $this->orderRepositoryService->all();
        return view("pages.admin.order.index", $data);
    }
    
    public function show($id)
    {
        # code...
    }

    public function approved($id)
    {
        $res = $this->orderRepositoryService->approved($id);
        $message = $res 
                    ? ["success" => "Sipariş başarı bir şekilde tamamlandı"]
                    : ["error" => "Siparişin tamamlanması yapılamadı. Tekrar deneyiniz"];
        return redirect()->route('admin.order.index')->with("message", $message);
    }

    public function unapproved($id)
    {
        $order = $this->orderRepositoryService->get($id);
        $res = $this->orderRepositoryService->unapproved($order->id);
        if($res){
            $this->productRepositoryService->increaseProductsCount($order);
        }
        $message = $res 
                    ? ["warning" => "Sipariş başarı bir şekilde iptal edildi."]
                    : ["error" => "Siparişin iptali yapılamadı. Tekrar deneyiniz"];
        return redirect()->route('admin.order.index')->with("message", $message);
    }

    
}
