<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Repository\CategoryRepository\ICategoryRepository;
use App\Repository\ProductRepository\IProductRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    private $productRepositoryService;
    private $categoryRepositoryService;
    public function __construct(
        IProductRepository $productRepositoryService,
        ICategoryRepository $categoryRepositoryService
    )
    {
        $this->productRepositoryService = $productRepositoryService;
        $this->categoryRepositoryService= $categoryRepositoryService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["products"] = $this->productRepositoryService->list();
        return view('pages.admin.product.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data["categories"] = $this->categoryRepositoryService->all();

        return view('pages.admin.product.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $requestData= $request->all();
        $path = Storage::putFile('public', $request->file('cover'));
        $requestData["cover"] = $path;
        
        $result = $this->productRepositoryService->store($requestData);
        $message = $result 
                    ? ["success" => "Ürün başarılı bir şekilde eklendi"] 
                    : ["error" => "Ürün eklenme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return redirect()->route('admin.product.create')->with('message', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data["categories"] = $this->categoryRepositoryService->all();
        $data["product"]    = $this->productRepositoryService->find($id);

        return view('pages.admin.product.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $requestData= $request->all();
        if($request->file('cover')) {
            Storage::delete('public', $request->cover_old_path);
            $path = Storage::putFile('public', $request->file('cover'));
            $requestData["cover"] = $path;
        }

        
        $result = $this->productRepositoryService->update($id, $requestData);
        $message = $result 
                    ? ["success" => "Ürün başarılı bir şekilde güncellendi"] 
                    : ["error" => "Ürün güncelleme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return redirect()->route('admin.product.edit', $id)->with('message', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produt = $this->productRepositoryService->find($id);
        Storage::delete('public', $produt->cover);
        $result = $this->productRepositoryService->destroy($id);
        $message = $result 
                    ? ["success" => "Ürün başarılı bir şekilde silindi"] 
                    : ["error" => "Ürün silme sırasıda bir hata oluştu. Tekrar deneyiniz"];
        return redirect()->route('admin.product.index')->with('message', $message);   
    }
}
