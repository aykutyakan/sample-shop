<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $productId = $this->route("id") ? $this->route("id") : 0;
        return [
            "category_id"=> "required|exists:categories,id",
            "name"     => "required|max:100|unique:products,name,".$productId.",id",
            "stock"     => "required|numeric|min:1",
            "cover"   => "required_without:cover_old_path|file|max:2000|mimes:jpeg,jpg,png",
            "price"      => "required|numeric|min:1",
            "discount"  => "nullable|numeric|between:0,99",
            "description"   => "required|min:100"
        ];
    }

    public function messages()
    {
        return [
            "category_id.required" => "Kategori alanı zorunludur.",
            "category_id.exists" => "Kategori DB kayıtlı değildir.",
            "stock.required" =>"Stok bilgisi boş bırakılamaz",
            "stock.numeric" =>"Stok bilgisi sayısal değerler olmalıdır.",
            "stock.min" =>"Stok miktari 0 dan büyük olmalı.",
            "name.required"=> "Ürün adı zorunludur.",
            "name.max"=> "Ürün adı en fazla 100 karakter olmalıdır.",
            "name.unique"=> "Ürün adı sistemde kayıtlıdır.",
            "cover.required_without"=> "Ürün resmi boş bırakılamaz",
            "cover.file"=> "Ürün resmi geçersiz",
            "cover.max"=> "Ürün resmi en fazla 2mb olmalıdır.",
            "cover.mimes"=> "Ürün resmi formatı geçersiz(Geçerli olanalar: jpeg,jpg,png).",
            "price.required"=> "Ürün fiyatı boş bırakılamaz",
            "price.numeric"=> "Ürün fiyatı sayısal olmalıdır",
            "price.min"=> "Ürün fiyatı en az 1₺ olmalıdır.",
            "discount.numeric" => "İndirim alanı geçersiz",
            "discount.between" => "İndirim oranı 0-100 arasında olmalı",
            "description.required"=> "Ürün açıklaması boş bırakılamaz",
            "description.min"=> "Ürün açıklaması en az 100 karakter girilmeli"
        ];
    }
}
