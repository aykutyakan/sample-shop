<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "buyed_name" => "required",
            "buyed_lastname" => "required",
            "city"=> "required",
            "address"=> "required",
            "post_code"=> "required|numeric",
            "phone"=> "required|numeric",
            "payment_type"=> "required|numeric",
        ];
    }
    public function messages() 
    {
        return [
            "buyed_name.required" => "Alıcı Adı boş bırakılamaz",
            "buyed_lastname.required" => "Alıcı soyadı boş bırakılamaz",
            "city.required" => "Şehir bilgisi boş bırakılamaz",
            "address.required" => "Adres bilgisi boş bırakılamaz",
            "post_code.required" => "Posta kodu bilgisi boş bırakılamaz",
            "post_code.numeric" => "Posta kodu bilgisi geçersiz",
            "phone.required|numeric" => "Telefon bilgisi boş bırakılamaz",
            "phone.numeric" => "Telefon bilgisi geçersiz",
            "payment_type.required" => "Ödeme türü boş bırakılamaz",
            "payment_type.numeric" => "Ödeme türü geçersiz",
        ];   
    }
}
