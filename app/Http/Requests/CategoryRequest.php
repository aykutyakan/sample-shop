<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $categoryId = $this->route("id") ? $this->route("id") : 0;
        return [
            "name" => "required|min:3|unique:categories,name,".$categoryId.",id",
            "label"=> "required|min:3"
        ];
    }

    public function messages()
    {
        return [
            "name.required" => "Kategori adı boş bırakılamaz",
            "name.min" => "Kategori adı en az 3 karakterden oluşmalıdır",
            "name.unique"=> "Girilen kategori adı sistemde kayıtlıdır.",
            "label.required"=> "Kategori etiketi boş bırakılamaz",
            "label.min"=> "Kategori etiketi en az 3 karakterden oluşmalıdır",
        ];
    }
}
