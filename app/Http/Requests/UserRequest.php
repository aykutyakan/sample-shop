<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|min:3",
            "email" => "required|email|unique:users,email",
            "password" => "required|min:6|confirmed",
        ];
    }

    public function messages()
    {
        return [
            "name.required" => "Kullanıcı adı boş bırakılamaz",
            "name.min"      => "Kullancı adı en az 3 karakterden oluşturmalıdır.",
            "email.required"=> "Email alanı boş bırakılamaz",
            "email.email"=> "Email alanı geçersiz formatta",
            "email.unique" => "Email adresi sistemde kayıtlıdır.",
            "password.required"=> "Şifre alanı boş bırakılamaz",
            "password.min"=> "Şifre alanı en az 6 karakterden oluşmalıdır.",
            "password.confirmed"=> "Şifreleriniz eşleşmiyor."
        ];
    }
}
