<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'stock', 'name', 'description', 'cover', 'price', 'discount',
    ];


    protected $with = ["category"];
    
    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function getPriceWithDiscountAttribute()
    {
        return $this->price - ($this->price*$this->discount/100);
    }
    public function getShortDescriptionAttribute()
    {
        return substr($this->description,0,50)."...";
    }

}
