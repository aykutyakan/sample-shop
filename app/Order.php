<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'buyed_name',  'buyed_lastname', 'city', 'post_code', 'address', 'phone', 'payment_type', 'is_approved'
    ];

    public $with = ["user", "orderProducts"];
    
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function orderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }
    public function getFullBuyedNameAttribute()
    {
        return $this->buyed_name." ".$this->buyed_lastname;
    }
}
