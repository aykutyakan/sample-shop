<?php

namespace App\Repository\CategoryRepository;

use App\Category;

class  ElequentCategoryRepository implements ICategoryRepository{
    private $model;

    public function __construct()
    {
        $this->model = new Category();
    }
    public function list()
    {
        return $this->model->paginate(15);
    }

    public function all()
    {
        return $this->model->all();
    }
    
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function update(int $id, array $data)
    {
        return $this->model->find($id)->update($data);
    }

    public function destroy(int $id)
    {
        return $this->model->find($id)->delete();
    }

}