<?php

namespace App\Repository\UserRepository;

use App\User;

class ElequentUserRepository implements IUserRepository{

    private $model;
    public function __construct()
    {
        $this->model = new User();
    }

    public function singupMember(array $data)
    {
        $data["password"] = bcrypt($data["password"]);
        $data["role"] = "ROLE_MEMBER";

        return $this->model->create($data);
    }
}