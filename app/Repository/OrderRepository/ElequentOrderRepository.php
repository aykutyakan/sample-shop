<?php

namespace App\Repository\OrderRepository;

use App\Enum\OrderStatus;
use App\Order;
use App\OrderProduct;
use Illuminate\Support\Facades\Auth;

class ElequentOrderRepository implements IOrderRepository{

    private $model;

    public function __construct()
    {
        $this->model = new Order();
    }

    public function list()
    {
        $userId = Auth::user()->id;
        return $this->model->where("user_id", $userId)->orderBy("is_approved")->get();
    }
    public function all()
    {
        return $this->model->orderBy("is_approved")->paginate(10);
    }
    public function get($id)
    {
        return $this->model->find($id);
    }
    public function order(array $data, $baskets)
    {
        $success = true;
        $data["user_id"] = Auth::user()->id;
        $order = $this->model->create($data);
        
        foreach($baskets as $basket)
        {
          $tmp = new OrderProduct();
          $tmp->order_id = $order->id;
          $tmp->product_id = $basket->product_id;
          $tmp->price = $basket->totalPriceWithDiscount;
          $tmp->count = $basket->count;
          if(!$tmp->save()) { $error= false; break;}
        }

        return $success; 

    }

    public function approved(int $id)
    {
        $order = $this->model->find($id);
        $order->is_approved = OrderStatus::APPROVED;
        return $order->save();
    }

    public function unapproved(int $id)
    {
        $order = $this->model->find($id);
        $order->is_approved = OrderStatus::UNAPPROVED;
        return $order->save();       
    }
}