<?php

namespace App\Repository\OrderRepository;

interface IOrderRepository{
    public function list();

    public function all();

    public function get($id);
    
    public function order(array $data, $baskets);

    public function approved(int $id);

    public function unapproved(int $id);

}