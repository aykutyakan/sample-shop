<?php

namespace App\Repository\ProductRepository;

interface IProductRepository{
    public function list();
    
    public function find(int $id);

    public function store(array $data);

    public function update(int $id, array $data);

    public function destroy(int $id);

    public function decreaseProductsCount($baskets);
    
    public function increaseProductsCount($baskets);

}