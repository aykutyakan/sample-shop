<?php

namespace App\Repository\ProductRepository;

use App\Product;

class ElequentProductRepository implements IProductRepository{
    private $model;

    public function __construct()
    {
        $this->model = new Product();
    }
    public function list() 
    {
        return $this->model->paginate(15);
    }

    public function find(int $id)
    {
        return $this->model->find($id);
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function update(int $id, array $data)
    {
        return $this->model->find($id)->update($data);
    }

    public function destroy(int $id)
    {
        return $this->model->find($id)->delete();
    }

    public function decreaseProductsCount($baskets)
    {
        foreach($baskets as $basket){
            $this->model->find($basket->product_id)
                ->decrement("stock", $basket->count);
        }
    }
    public function increaseProductsCount($order)
    {
        foreach($order->orderProducts as $orderProduct){
            $this->model->find($orderProduct->product_id)
                ->increment("stock", $orderProduct->count);
        }
    }
    public function checkProductStock($baskets)
    {
        $ids= $baskets->map->only(['product_id'])->toArray();
        $products = $this->model->whereIn("id", $ids)->get();
        $founded= null;
        foreach($baskets as $basket){
            $founded = $products->find($basket->product_id)->stock - $basket->count < 0 ? $basket : null;
            if($founded) 
                break;
        }
        return $founded;
    }

}