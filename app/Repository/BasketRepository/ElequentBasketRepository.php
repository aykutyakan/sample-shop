<?php

namespace App\Repository\BasketRepository;

use App\Basket;
use Illuminate\Support\Facades\Auth;

class ElequentBasketRepository implements IBasketRepository{

    private $model;
    public function __construct()
    {
        $this->model = new Basket();
    }
    public function all()
    {
        $arr = [
            "user_id" => Auth::user()->id,
        ];
        return $this->model->where($arr)->get();
    }
    
    public function add(int $productId, int $count)
    {
        $arr = [
            "user_id" => Auth::user()->id,
            "product_id"=> $productId,
        ];
        $basket = $this->model->firstOrNew($arr);
        $basket->count = $basket->count 
                            ? $basket->count + $count
                            : $count;
        return $basket->save();
    }

    public function drop(int $productId, int $count)
    {
        $arr = [
            "user_id" => Auth::user()->id,
            "product_id"=> $productId,
        ];
        $basket = $this->model->where($arr)->first();
        if($basket){
            $basket->count -=  $count;
            if($basket->count == 0) $basket->delete();
            else  $basket->save();
            return true;
        } 
        return false;
    }
    public function flush()
    {
        $arr = [
            "user_id" => Auth::user()->id,
        ];
        return $this->model->where($arr)->delete();
    }

}