<?php

namespace App\Repository\BasketRepository;

interface IBasketRepository{

    public function all();
    
    public function add(int $productId, int $count);

    public function drop(int $productId, int $count);

    public function flush();
}